//
//  BackgroundLayer.m
//  SpaceViking
//
//  Created by Brandon Levasseur on 8/4/13.
//  Copyright (c) 2013 Brandon Levasseur. All rights reserved.
//

#import "BackgroundLayer.h"
#import "cocos2d.h"

@implementation BackgroundLayer

-(id)init
{
    self = [super init];
    if (self) {
        CCSprite *backgroundImage;
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            backgroundImage = [CCSprite spriteWithFile:@"background.png"];
        } else {
            backgroundImage = [CCSprite spriteWithFile:@"backgroundiPhone.png"];
        }
        
        CGSize screenSize = [[CCDirector sharedDirector] winSize];
        [backgroundImage setPosition:CGPointMake(screenSize.width/2, screenSize.height/2)];
        [self addChild:backgroundImage z:0 tag:0];
    }
    
    return self;
}

@end
