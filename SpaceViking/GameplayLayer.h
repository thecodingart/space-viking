//
//  GameplayLayer.h
//  SpaceViking
//
//  Created by Brandon Levasseur on 8/4/13.
//  Copyright (c) 2013 Brandon Levasseur. All rights reserved.
//

#import "CCLayer.h"
#import "cocos2d.h"

@class SneakyButton;
@class SneakyJoystick;


@interface GameplayLayer : CCLayer {
    CCSprite *vikingSprite;
    SneakyJoystick *leftJoyStick;
    SneakyButton *jumpButton;
    SneakyButton *attackButton;
}

@end
