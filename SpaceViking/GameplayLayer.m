//
//  GameplayLayer.m
//  SpaceViking
//
//  Created by Brandon Levasseur on 8/4/13.
//  Copyright (c) 2013 Brandon Levasseur. All rights reserved.
//

#import "GameplayLayer.h"
#import "SneakyButtonSkinnedBase.h"
#import "SneakyButton.h"
#import "SneakyJoystickSkinnedBase.h"
#import "SneakyJoystick.h"


@implementation GameplayLayer

-(void)initJoystickAndButtons
{
    CGSize screenSize = [[CCDirector sharedDirector] winSize];
    CGRect joystickBaseDimensions = CGRectMake(0, 0, 128.0f, 128.0f);
    CGRect jumpButtonDimensions = CGRectMake(0, 0, 64.0f, 64.0f);
    CGRect attackButtonDimensions = CGRectMake(0, 0, 64.0f, 64.0f);
    CGPoint joystickBasePosition;
    CGPoint jumpButtonPosition;
    CGPoint attackButtonPosition;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        CCLOG(@"Positioning Joystick and Buttons for iPad");
        joystickBasePosition = CGPointMake(screenSize.width * 0.0625f, screenSize.height * 0.052f);
        jumpButtonPosition = CGPointMake(screenSize.width * 0.946f, screenSize.height * 0.052f);
        attackButtonPosition = CGPointMake(screenSize.width * 0.947f, screenSize.height * 0.169f);
    } else {
        CCLOG(@"Positioning Joystick and Buttons for iPhone");
        joystickBasePosition = CGPointMake(screenSize.width * 0.07f, screenSize.height * 0.11f);
        jumpButtonPosition = CGPointMake(screenSize.width * 0.93f, screenSize.height * 0.11f);
        attackButtonPosition = CGPointMake(screenSize.width * 0.93f, screenSize.height * 0.35f);
    }
    
    SneakyJoystickSkinnedBase *joystickBase = [[SneakyJoystickSkinnedBase alloc]init];
    joystickBase.position = joystickBasePosition;
    joystickBase.backgroundSprite = [CCSprite spriteWithFile:@"dpadDown.png"];
    joystickBase.thumbSprite = [CCSprite spriteWithFile:@"joystickDown.png"];
    joystickBase.joystick = [[SneakyJoystick alloc]initWithRect:joystickBaseDimensions];
    leftJoyStick = joystickBase.joystick;
    [self addChild:joystickBase];
    
    SneakyButtonSkinnedBase *jumpButtonBase = [[SneakyButtonSkinnedBase alloc]init];
    jumpButtonBase.position = jumpButtonPosition;
    jumpButtonBase.defaultSprite = [CCSprite spriteWithFile:@"jumpUp.png"];
    jumpButtonBase.pressSprite = [CCSprite spriteWithFile:@"jumpDown.png"];
    jumpButtonBase.button = [[SneakyButton alloc]initWithRect:jumpButtonDimensions];
    jumpButton = jumpButtonBase.button;
    jumpButton.isToggleable = NO;
    [self addChild:jumpButtonBase];
    
    SneakyButtonSkinnedBase *attackButtonBase = [[SneakyButtonSkinnedBase alloc]init];
    attackButtonBase.position = attackButtonPosition;
    attackButtonBase.defaultSprite = [CCSprite spriteWithFile:@"handUp.png"];
    attackButtonBase.activatedSprite = [CCSprite spriteWithFile:@"handDown.png"];
    attackButtonBase.pressSprite = [CCSprite spriteWithFile:@"handDown.png"];
    attackButtonBase.button = [[SneakyButton alloc]initWithRect:attackButtonDimensions];
    attackButton = attackButtonBase.button;
    attackButton.isToggleable = NO;
    [self addChild:attackButtonBase];
}

-(void)applyJoystick:(SneakyJoystick *)aJoystick toNode:(CCNode *)tempNode forTimeDelta:(float)deltaTime
{
    CGPoint scaledVelocity = ccpMult(aJoystick.velocity, 1024.0f); // * size of the iPad screen
    
    CGPoint newPosition = CGPointMake(tempNode.position.x + scaledVelocity.x * deltaTime, tempNode.position.y + scaledVelocity.y * deltaTime);
    
    [tempNode setPosition:newPosition];
    
    if (jumpButton.active == YES) {
        CCLOG(@"Jump button is pressed");
    }
    if (attackButton.active == YES) {
        CCLOG(@"Attack button is pressed");
    }
}

-(void)update:(ccTime)deltaTime
{
    [self applyJoystick:leftJoyStick toNode:vikingSprite forTimeDelta:deltaTime];
}

-(id)init
{
    self = [super init];
    if (self) {
        CGSize screenSize = [[CCDirector sharedDirector] winSize];
        [self setTouchEnabled:YES];
//        vikingSprite = [CCSprite spriteWithFile:@"sv_anim_1.png"];
        CCSpriteBatchNode *chapter2SpritebatchNode;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"scene1atlas.plist"];
            chapter2SpritebatchNode = [CCSpriteBatchNode batchNodeWithFile:@"scene1atlas.png"];
        } else {
            [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"scene1atlasiPhone.plist"];
            chapter2SpritebatchNode = [CCSpriteBatchNode batchNodeWithFile:@"scene1atlasiPhone.png"];
        }
        vikingSprite = [CCSprite spriteWithSpriteFrameName:@"sv_anim_1.png"];
        
        [chapter2SpritebatchNode addChild:vikingSprite];
        [self addChild:chapter2SpritebatchNode];
        
        [vikingSprite setPosition:CGPointMake(screenSize.width /2, screenSize.height *0.17f)];
        
//        NSArray *spriteFrames = @[[[CCSpriteFrameCache sharedSpriteFrameCache]spriteFrameByName:@"sv_anim_2.png"],[[CCSpriteFrameCache sharedSpriteFrameCache]spriteFrameByName:@"sv_anim_3.png"],[[CCSpriteFrameCache sharedSpriteFrameCache]spriteFrameByName:@"sv_anim_4.png"]];
//        CCAnimation *exampleAnim = [[CCAnimation alloc] initWithSpriteFrames:spriteFrames delay:0.5f];
//        exampleAnim.restoreOriginalFrame = NO;
//        
//        CCActionInterval *animateAction = [CCAnimate actionWithAnimation:exampleAnim];
//        CCAction *repeatAction = [CCRepeatForever actionWithAction:animateAction];
//        [vikingSprite runAction:repeatAction];
        
        [self initJoystickAndButtons];
        [self scheduleUpdate];
        
        if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad) {
            [vikingSprite setScaleX:screenSize.width/1024.0f];
            [vikingSprite setScaleY:screenSize.height/768.0f];
        }
    }
    return self;
}

@end
