//
//  main.m
//  SpaceViking
//
//  Created by Brandon Levasseur on 8/4/13.
//  Copyright Brandon Levasseur 2013. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
    
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    int retVal = UIApplicationMain(argc, argv, nil, @"AppController");
    [pool release];
    return retVal;
}
